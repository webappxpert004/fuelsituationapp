package com.fuelsituation.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;

import com.fuelsituation.R;
import com.google.android.material.circularreveal.cardview.CircularRevealCardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

//import butterknife.BindView;

public class SelectActivity extends AppCompatActivity {

    @BindView(R.id.card_petrol)
    CardView card_petrol;
    @BindView(R.id.card_kero)
    CardView card_kero;
    @BindView(R.id.card_diesel)
    CardView card_diesel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        ButterKnife.bind(this);
    }

    @OnCheckedChanged(R.id.card_diesel)
    public void onDieselClicked(){
        Intent i = new Intent(SelectActivity.this, NearByPumpsActivity.class);
        i.putExtra("fuel", "Diesel");
        startActivity(i);
    }

    @OnCheckedChanged(R.id.card_petrol)
    public void onPetrolClicked(){
        Intent i = new Intent(SelectActivity.this, NearByPumpsActivity.class);
        i.putExtra("fuel", "Petrol");
        startActivity(i);
    }

    @OnCheckedChanged(R.id.card_kero)
    public void onKeroClicked(){
        Intent i = new Intent(SelectActivity.this, NearByPumpsActivity.class);
        i.putExtra("fuel", "Kero");
        startActivity(i);
    }
}
