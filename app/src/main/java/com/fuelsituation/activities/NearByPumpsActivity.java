package com.fuelsituation.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.fuelsituation.R;
import com.fuelsituation.adapters.NearByAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NearByPumpsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private NearByAdapter nearByAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by_pumps);
        ButterKnife.bind(this);

        setAdapter();
    }

    public void setAdapter()
    {
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(NearByPumpsActivity.this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(gridLayoutManager);

        nearByAdapter = new NearByAdapter(NearByPumpsActivity.this, null);
        recyclerView.setAdapter(nearByAdapter);
    }
}
